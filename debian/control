Source: btag
Section: sound
Priority: optional
Maintainer: Fernando Tarlá Cardoso Lemos <fernandotcl@gmail.com>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 libboost-dev,
 libboost-filesystem-dev,
 libboost-locale-dev,
 libcue-dev,
 libedit-dev,
 libtag-dev,
 pkgconf,
 zlib1g-dev,
Standards-Version: 4.7.0
Homepage: https://github.com/fernandotcl/btag
Vcs-Git: https://salsa.debian.org/debian/btag.git
Vcs-Browser: https://salsa.debian.org/debian/btag

Package: btag
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: interactive command-line based multimedia tag editor
 btag is an interactive utility for tagging multimedia files in batches.
 It tries to automate most of the process by stripping away stray whitespace
 guessing the best capitalization style for the text fields and keeping
 state on the current album being tagged.
 .
 btag can change the capitalization of text fields even when those contain
 non-ASCII characters, relying on the standard library to perform the
 appropriate case conversions. It works on directories containing complete
 albums as well as on individual files.
 .
 All tag formats supported by TagLib should be supported by btag. This
 includes (among others) ID3v1 and ID3v2 tags found in MP3 files, Ogg
 Vorbis comments, ID3 tags and Vorbis comments in FLAC files.
